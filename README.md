
# ipmiproxy
## Description
`ipmiproxy` provides efficent command line parameters auto-discovery for `ipmitool`.

It handles the command output directly from stdout buffer, and manages the thread to ensure fast reaction to errors and minimize the overhead of reattempting a connection.

## CLI exceptions
### Missing LAN+ channel
**Condition:** `ipmitool` failing with message `Authentication type NONE`

**Action:** re-execute `ipmitool` with `-L` parameter

In cases in which, usually for security reasons, IPMIv1.5 is not available on the BMC, the command will be re-attempted by enabling IPMIv2 (LAN+)

### Unable to discover proper cipher set
**Condition:** `ipmitool` failing with message `Error in open session response message : insufficient resources for session`

**Action:** re-execute `ipmitool` with `-C 17` parameter

In some cases, `ipmitool` will fail to detect the proper cipher set to use with BMCs. The command will be re-attempted by manually forcing the most safe cipher.
